import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class BugsManagerTests {
//    WebDriver webDriver = new ChromeDriver();
        WebDriver webDriver = new FirefoxDriver();

    MainPage mainPage = new MainPage(webDriver);

    String name = "Some name of bug";
    String note = "Some notes";
    String priority = "1";

    String NameInLine = "Some name of bug in line";
    String NoteInLine = "Some notes of bug in line";

    @Before
    public void setUp(){

        webDriver.get("https://autotesttask.herokuapp.com/");

    }

    @Test
    public void SearchFirstBugString(){
        WebElement foundBug = mainPage.selectSearchableLineByBugName("First bug");
        Assert.assertTrue(webDriver.getPageSource().contains("First bug"));
    }

    @Test
    public void AddBugWithStdForm() throws InterruptedException {

        mainPage.callTheAddBugForm();
        mainPage.enterNameInForm(name);
        mainPage.enterNoteInForm(note);
        mainPage.enterPriorityInForm(priority);

        mainPage.clickOkButtonInAddBugForm();

        Thread.sleep(2000);

        Assert.assertTrue(webDriver.getPageSource().contains(name));
        Assert.assertTrue(webDriver.getPageSource().contains(note));
        Assert.assertTrue(webDriver.getPageSource().contains(priority));

        mainPage.selectSearchableLineByBugName(name);
        Thread.sleep(1000);

        mainPage.clickDeleteButton();
        Thread.sleep(1000);

        mainPage.clickRefreshButton();
        Thread.sleep(1000);

        Assert.assertFalse(webDriver.getPageSource().contains(name));
    }

    @Test
    public void AddBugFromString() throws InterruptedException {
        mainPage.clickAddInLineButton();
        Thread.sleep(500);
        mainPage.enterNameInLine(NameInLine);
        Thread.sleep(500);
        mainPage.enterNoteInLine(NoteInLine);
        Thread.sleep(500);
        mainPage.clickApplyButton();

        Assert.assertTrue(webDriver.getPageSource().contains(NameInLine));
        Assert.assertTrue(webDriver.getPageSource().contains(NoteInLine));
        Assert.assertTrue(webDriver.getPageSource().contains("0"));

        mainPage.selectSearchableLineByBugName(NameInLine);
        Thread.sleep(1000);

        mainPage.clickDeleteButton();
        Thread.sleep(1000);

        mainPage.clickRefreshButton();
        Thread.sleep(1000);

        Assert.assertFalse(webDriver.getPageSource().contains(NameInLine));
    }

    @After
    public void cleanUp(){
        webDriver.quit();
    }
}
