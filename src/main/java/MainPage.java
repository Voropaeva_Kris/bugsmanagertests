import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static com.sun.jna.platform.win32.COM.tlb.imp.TlbBase.TAB;

public class MainPage {
    private WebDriver webDriver;
    public WebElement webElement;

    @FindBy(id = "button-1047")
    public WebElement addInFormButton;

    @FindBy(name = "name")
    public WebElement nameFieldInForm;

    @FindBy(name = "notes")
    public WebElement noteFieldInForm;

    @FindBy(name = "priority")
    public WebElement priorityFieldInForm;

    @FindBy(css = "button[data-qtip='OK']")
    public WebElement okButtonInForm;

    @FindBy(id = "button-1042")
    public WebElement addInLineButton;

    @FindBy(css = "input[type=text]")
    public WebElement nameInLineField;

    @FindBy(css = "textarea[role='textbox']")
    public WebElement noteInLineField;

    @FindBy(id = "button-1044")
    public WebElement applyButton;

    @FindBy(id = "button-1040")
    public WebElement refreshButton;

    @FindBy (id = "button-1045")
    public WebElement deleteButton;

    @FindBy(id = "button-1010")
    WebElement yesButton;



    public MainPage(WebDriver driver){

        webDriver = driver;

        PageFactory.initElements(webDriver, this);

    }

    public void callTheAddBugForm() throws InterruptedException {
        addInFormButton.click();
        Thread.sleep(2000);

    }
    public void enterNameInForm(String name) throws InterruptedException {

        nameFieldInForm.click();
        nameFieldInForm.clear();
        nameFieldInForm.sendKeys(name);
    }
    public void enterNoteInForm(String note) throws InterruptedException {


        noteFieldInForm.click();
        noteFieldInForm.clear();
        noteFieldInForm.sendKeys(note);

    }
    public void enterPriorityInForm(String priority) throws InterruptedException {

        priorityFieldInForm.click();
        priorityFieldInForm.clear();
        priorityFieldInForm.sendKeys(priority);
    }
    public void clickOkButtonInAddBugForm() {
        okButtonInForm.click();
    }
    public void clickAddInLineButton() throws InterruptedException{
        addInLineButton.click();
        Thread.sleep(2000);
    }
    public void enterNameInLine(String name) {
        nameInLineField.click();
        nameInLineField.clear();
        nameInLineField.sendKeys(name);
        nameInLineField.sendKeys(TAB);

    }
    public void enterNoteInLine(String note){
        noteInLineField.click();
        noteInLineField.clear();
        noteInLineField.sendKeys(note);
        noteInLineField.sendKeys(TAB);
    }
    public void clickApplyButton() throws InterruptedException {
        applyButton.click();
        Thread.sleep(1000);
    }
    public void clickRefreshButton() {
        refreshButton.click();
    }
    public WebElement selectSearchableLineByBugName(String name){
        WebElement table = webDriver.findElement(By.id("gridview-1028"));
        List<WebElement> lines = table.findElements(By.tagName("td"));

        for (WebElement element: lines){
            if(element.getText().equals(name))
                {
                    //System.out.println(element.getText());
                    element.click();
                }
        }
        return table;
    }
    public void clickDeleteButton() throws InterruptedException {
        deleteButton.click();
        Thread.sleep(2000);

        yesButton.click();
    }
}